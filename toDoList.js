// If an li element is clicked, toggle the class "done" on the <li>
const toggleElement = function toggleElement(e){
	if (e.target.className !== 'done') {
			e.target.className = 'done';
	}
	else{
		e.target.className = 'active';
	}

}
// If a delete link is clicked, delete the li element / remove from the DOM
const deleteElement = function deleteElement(e){
	if(e.target !== e.currentTarget){
		let clickedli = e.target.parentNode;
	
		clickedli.parentNode.removeChild(clickedli);
	}
	e.stopPropagation();
}
// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!
// Make sure to add an event listener(s) to your new <li> (if needed)
const addListItem = function(e) {
    e.preventDefault();
    let ulelement = document.getElementsByTagName('ul')[0];
    //const input = this.parentNode.getElementsByTagName('input')[0];
    const input = document.getElementsByTagName('input')[0];
	console.log(this.input);
    const text = input.value; // use this text to create a new <li>
    const newli = document.createElement('li');
	const newspan = document.createElement('span');
	//newli.setAttribute('span', text);
	const spantext = document.createTextNode(text);
	newspan.appendChild(spantext);
	newli.appendChild(newspan);
	const aelement =  document.createElement('a'); 
	const textelement = document.createTextNode("Delete");
	aelement.className = "delete";
	aelement.appendChild(textelement);
	newli.appendChild(aelement);
	ulelement.appendChild(newli);
	input.value = '';
  // Finish function here
};
 const addelement = document.getElementsByClassName("add-item");
 addelement[0].addEventListener('click', (event) => {
	addListItem(event);
  });
const deleteelement = document.getElementsByClassName("today-list");
deleteelement[0].addEventListener('click', (event) => {
	let target = event.target;
	if( target.tagName === 'A'){
		deleteElement(event);
	}
	if( target.tagName === 'LI' ){
		toggleElement(event);
	}
	
	
	/*{
	let linode = target;
	let parentnode = target.parentNode;
	const childrens = Array.from(linode.childNodes);
	childrens.forEach(function(item){
		linode.removeChild(item);
	});
	parentnode.removeChild(linode);
	}*/
	});
	
	

