// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let melement = document.getElementsByTagName('body')[0];
let pelements = document.getElementsByTagName('p')[0];
const aeement = document.createElement('a');
const textelement = document.createTextNode("Buy Now!");
aeement.setAttribute('id', 'cta');
let ielement = document.getElementById('cta');
aeement.appendChild(textelement);
melement.appendChild(aeement);
melement.insertBefore(pelements, aeement);

// Access (read) the data-color attribute of the <img>,
// log to the console
let imgelement = document.body.getElementsByTagName('img')[0];
console.log("Data color attribute of image is " + imgelement.getAttribute('data-color'));


// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
let lielement = document.body.getElementsByTagName("li")[2];
lielement.setAttribute('className',"highlight");

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
melement.removeChild(pelements);