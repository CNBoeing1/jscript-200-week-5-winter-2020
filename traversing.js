// Given the <body> element as variable body,
// access the <main> node and log to the console.
const body = document.querySelector('body');
const childrens = body.childNodes;
let i = 0;
while(childrens[i].nodeName == "#text"){
	i++;
	continue;
}
console.log("IN " + childrens[i].nodeName); 


// Given the <ul> element as variable ul,
// access <body>  and log to the console.
const ul = document.querySelector('ul');
let temp = ul.previousSibling;
while(temp.tagName !== "BODY"){
	temp = temp.parentNode;
}
console.log("IN " + temp.nodeName);

// Given the <p> element as var p,
// access the 3rd <li>  and log to the console.
const p = document.querySelector('p');
let uelement = p.previousSibling;
while(uelement.nodeName == "#text"){
	uelement = uelement.previousSibling;
}

let pelement = uelement.lastChild;

while(pelement.nodeName == "#text" && pelement.textContent.trim().length == 0){
	pelement = pelement.previousSibling;
}
const liel = pelement.childNodes;
for(let k = 0; k < liel.length; k++){
	console.log(liel[k].textContent);
}
