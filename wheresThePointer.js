// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click
let fields = document.querySelectorAll("td");
for(let field of Array.from(fields)){ 
	field.addEventListener("click", (event) => {
		let text = event.target;
		text.innerHTML = "X is at " + event.clientX + " and Y is at " + event.clientY;
		
	});

}