// When a user clicks the + element, the count should increase by 1 on screen.
// When a user clicks the – element, the count should decrease by 1 on screen.
 
 let counter = 0;
 const clickplus = document.getElementById('plus');
 const countertext = document.getElementById('cnt');
 countertext.innerHTML = counter;
 const clickminus = document.getElementById('minus');

 clickplus.addEventListener('click', (event) => {
	counter++;
	countertext.innerHTML = counter;
 });
 clickminus.addEventListener('click', (event) => {
	if(counter > 0)
		counter--;
	countertext.innerHTML = counter;
 });