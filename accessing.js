// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"
let changeheader = document.getElementById("weather-head");
changeheader.innerText = "February 10 Weather Forecast\, Seattle";

// Change the styling of every element with class "sun" to set the color to "orange"
let changeclass = document.getElementsByClassName('sun');
for(let i=0; i<changeclass.length; i++){
	changeclass[i].style.color = 'orange';
}

// Change the class of the second <li> from to "sun" to "cloudy"
let changeli = document.getElementsByTagName('li')[1];
changeli.classList.remove('sun');
changeli.classList.add('cloudy');